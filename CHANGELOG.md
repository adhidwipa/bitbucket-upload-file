# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.3.2

- patch: Internal maintenance: add bitbucket-pipe-release.

## 0.3.1

- patch: Fix max files limit.

## 0.3.0

- minor: Internal maintenance: bump bitbucket-pipes-toolkit version.

## 0.2.0

- minor: Add support for uploading multiple files.

## 0.1.8

- patch: Internal maintenance: change pipe metadata according to new structure

## 0.1.7

- patch: Internal maintenance: remove version digest from image

## 0.1.6

- patch: Internal maintenance: fix version parsing

## 0.1.5

- patch: Internal maintenance: Update release process.

## 0.1.4

- patch: Internal maintenance: Add gitignore secrets.

## 0.1.3

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.

## 0.1.2

- patch: Add warning message about new version of the pipe available.

## 0.1.1

- patch: Internal maintenance: update pipes toolkit version

## 0.1.0

- minor: Initial release

